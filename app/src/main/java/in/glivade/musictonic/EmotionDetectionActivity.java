package in.glivade.musictonic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.affectiva.android.affdex.sdk.Frame;
import com.affectiva.android.affdex.sdk.detector.CameraDetector;
import com.affectiva.android.affdex.sdk.detector.Detector;
import com.affectiva.android.affdex.sdk.detector.Face;

import java.util.List;

import in.glivade.musictonic.util.Const;

public class EmotionDetectionActivity extends AppCompatActivity
        implements Detector.ImageListener, CameraDetector.CameraEventListener {

    SurfaceView cameraPreview;
    RelativeLayout mainLayout;
    CameraDetector detector;
    int previewWidth = 0;
    int previewHeight = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emotion_detection);
        mainLayout = findViewById(R.id.activity_emotion_detection);
        cameraPreview = new SurfaceView(this) {
            @Override
            public void onMeasure(int widthSpec, int heightSpec) {
                int measureWidth = MeasureSpec.getSize(widthSpec);
                int measureHeight = MeasureSpec.getSize(heightSpec);
                int width;
                int height;
                if (previewHeight == 0 || previewWidth == 0) {
                    width = measureWidth;
                    height = measureHeight;
                } else {
                    float viewAspectRatio = (float) measureWidth / measureHeight;
                    float cameraPreviewAspectRatio = (float) previewWidth / previewHeight;

                    if (cameraPreviewAspectRatio > viewAspectRatio) {
                        width = measureWidth;
                        height = (int) (measureWidth / cameraPreviewAspectRatio);
                    } else {
                        width = (int) (measureHeight * cameraPreviewAspectRatio);
                        height = measureHeight;
                    }
                }
                setMeasuredDimension(width, height);
            }
        };
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        cameraPreview.setLayoutParams(params);
        mainLayout.addView(cameraPreview, 0);

        detector = new CameraDetector(this, CameraDetector.CameraType.CAMERA_FRONT, cameraPreview);
        detector.setDetectJoy(true);
        detector.setDetectSadness(true);
        detector.setDetectFear(true);
        detector.setDetectAnger(true);
        detector.setDetectSurprise(true);
        detector.setDetectDisgust(true);
        detector.setImageListener(this);
        detector.setOnCameraEventListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startDetector();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopDetector();
    }

    void startDetector() {
        if (!detector.isRunning()) {
            detector.start();
        }
    }

    void stopDetector() {
        if (detector.isRunning()) {
            detector.stop();
        }
    }

    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void onCameraSizeSelected(int width, int height, Frame.ROTATE rotate) {
        if (rotate == Frame.ROTATE.BY_90_CCW || rotate == Frame.ROTATE.BY_90_CW) {
            previewWidth = height;
            previewHeight = width;
        } else {
            previewHeight = height;
            previewWidth = width;
        }
        cameraPreview.requestLayout();
    }

    @Override
    public void onImageResults(List<Face> list, Frame frame, float v) {
        if (list == null) {
            return;
        }

        if (list.size() == 0) {
            return;
        }

        Face face = list.get(0);

        if (face.emotions.getJoy() > 30) {
            stopDetector();
            Intent intent = new Intent(EmotionDetectionActivity.this, MusicPlayerActivity.class);
            intent.putExtra("emotion", 0);
            intent.putExtra("mood", Const.EMOTIONS[0]);
            startActivity(intent);
        } else if (face.emotions.getSadness() > 30) {
            stopDetector();
            Intent intent = new Intent(EmotionDetectionActivity.this, MusicPlayerActivity.class);
            intent.putExtra("emotion", 1);
            intent.putExtra("mood", Const.EMOTIONS[1]);
            startActivity(intent);
        } else if (face.emotions.getAnger() > 30) {
            stopDetector();
            Intent intent = new Intent(EmotionDetectionActivity.this, MusicPlayerActivity.class);
            intent.putExtra("emotion", 2);
            intent.putExtra("mood", Const.EMOTIONS[2]);
            startActivity(intent);
        } else if (face.emotions.getFear() > 30) {
            stopDetector();
            Intent intent = new Intent(EmotionDetectionActivity.this, MusicPlayerActivity.class);
            intent.putExtra("emotion", 3);
            intent.putExtra("mood", Const.EMOTIONS[3]);
            startActivity(intent);
        } else if (face.emotions.getSurprise() > 30) {
            stopDetector();
            Intent intent = new Intent(EmotionDetectionActivity.this, MusicPlayerActivity.class);
            intent.putExtra("emotion", 4);
            intent.putExtra("mood", Const.EMOTIONS[4]);
            startActivity(intent);
        } else if (face.emotions.getDisgust() > 30) {
            stopDetector();
            Intent intent = new Intent(EmotionDetectionActivity.this, MusicPlayerActivity.class);
            intent.putExtra("emotion", 5);
            intent.putExtra("mood", Const.EMOTIONS[5]);
            startActivity(intent);
        }
    }
}
