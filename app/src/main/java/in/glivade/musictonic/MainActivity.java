package in.glivade.musictonic;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.glivade.musictonic.db.DatabaseHandler;
import in.glivade.musictonic.util.Const;
import in.glivade.musictonic.util.MyPreference;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Button button;
    private CoordinatorLayout coordinatorLayout;
    private FloatingActionButton fab;
    private TextView textViewName;
    private Intent intent;
    private DatabaseHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        handler = new DatabaseHandler(getApplicationContext());

        coordinatorLayout = findViewById(R.id.activity_main);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasStoragePermission()) {
                    intent = new Intent(MainActivity.this, SongsListActivity.class);
                    startActivity(intent);
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 4);
                }
            }
        });

        button = findViewById(R.id.take_snap);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasCameraPermission() && hasSmsPermission()) {
                    if (checkPlaylist()) {
                        Intent intent = new Intent(MainActivity.this, EmotionDetectionActivity.class);
                        startActivity(intent);
                    } else Snackbar.make(coordinatorLayout,
                            "Add at least a song to each playlist", Snackbar.LENGTH_LONG).show();
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.SEND_SMS}, 56);
                }
            }
        });

        textViewName = findViewById(R.id.txt_name);
        String welcome = "Welcome, " + new MyPreference(this).getName();
        textViewName.setText(welcome);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_manage_contacts) {
            startActivity(new Intent(this, ContactActivity.class));
        } else if (item.getItemId() == R.id.action_logout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Logout");
            builder.setMessage("Are you sure you want to logout?");
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new MyPreference(MainActivity.this).clearUser();
                    startActivity(new Intent(MainActivity.this, SplashActivity.class));
                    finish();
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
        return true;
    }

    private boolean checkPlaylist() {
        for (int i = 0; i < Const.EMOTIONS.length; i++) {
            int count;
            count = handler.getSongsCount(Const.EMOTIONS[i]);
            if (count < 1)
                return false;
        }
        return true;
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean hasSmsPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean hasCameraPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }
}
