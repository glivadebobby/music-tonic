package in.glivade.musictonic.util;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPreference {

    private static final String PREF_USER = "user";
    private static final String NAME = "name";
    private static final String IS_LOGGED_IN = "is_logged_in";
    private SharedPreferences preferencesUser;

    public MyPreference(Context context) {
        preferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
    }

    public String getName() {
        return preferencesUser.getString(NAME, null);
    }

    public void setName(String name) {
        preferencesUser.edit().putString(NAME, name).apply();
    }

    public boolean getIsLoggedIn() {
        return preferencesUser.getBoolean(IS_LOGGED_IN, false);
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        preferencesUser.edit().putBoolean(IS_LOGGED_IN, isLoggedIn).apply();
    }

    public void clearUser() {
        preferencesUser.edit().clear().apply();
    }
}
