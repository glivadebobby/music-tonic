package in.glivade.musictonic.util;

import in.glivade.musictonic.R;

/**
 * Created by Bobby on 17-10-2016
 */

public class Const {
    public static final String EMOTIONS[] = {"Happy", "Sadness", "Anger", "Fear",
            "Surprise", "Disgust"};
    public static final String TABS[] = {"HAPPY", "SADNESS", "ANGER", "FEAR",
            "SURPRISE", "DISGUST"};
    public static final Integer ICONS[] = {R.drawable.smile, R.drawable.sad,
            R.drawable.angry, R.drawable.worried, R.drawable.surprised,
            R.drawable.speechless};
}
