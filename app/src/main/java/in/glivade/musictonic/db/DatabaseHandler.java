package in.glivade.musictonic.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import in.glivade.musictonic.model.Contact;
import in.glivade.musictonic.model.SongItem;
import in.glivade.musictonic.util.Const;

/**
 * Created by Bobby on 17-10-2016
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "Playlist";
    private static final String TABLE_USER = "user";
    private static final String TABLE_CONTACT = "contact";
    private static final String KEY_ID = "id";
    private static final String KEY_ALBUM_ID = "album_id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_ALBUM = "album";
    private static final String KEY_ARTIST = "artist";
    private static final String KEY_DURATION = "duration";
    private static final String KEY_PATH = "path";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_PASS = "pass";
    private static final String KEY_EMOTION_ID = "emotion_id";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + " ("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT," + KEY_PHONE + " TEXT," + KEY_PASS + " TEXT" + ")";
        db.execSQL(CREATE_USER_TABLE);
        String CREATE_CONTACT_TABLE = "CREATE TABLE " + TABLE_CONTACT + " ("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_EMOTION_ID + " INTEGER,"
                + KEY_NAME + " TEXT," + KEY_PHONE + " TEXT" + ")";
        db.execSQL(CREATE_CONTACT_TABLE);
        for (int i = 0; i < Const.EMOTIONS.length; i++) {
            String CREATE_TABLE = "CREATE TABLE " + Const.EMOTIONS[i] + " ("
                    + KEY_ID + " INTEGER PRIMARY KEY," + KEY_ALBUM_ID + " INTEGER,"
                    + KEY_TITLE + " TEXT," + KEY_ALBUM + " TEXT," + KEY_ARTIST + " TEXT,"
                    + KEY_DURATION + " INTEGER," + KEY_PATH + " TEXT" + ")";
            db.execSQL(CREATE_TABLE);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);
        for (int i = 0; i < Const.EMOTIONS.length; i++)
            db.execSQL("DROP TABLE IF EXISTS " + Const.EMOTIONS[i]);
        onCreate(db);
    }

    public void addUser(String name, String email, String phone, String pass) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_EMAIL, email);
        values.put(KEY_PHONE, phone);
        values.put(KEY_PASS, pass);

        db.insert(TABLE_USER, null, values);
        db.close();
    }

    public String signIn(String phone, String pass) {
        String selectQuery = "SELECT * FROM " + TABLE_USER + " WHERE phone = ? AND pass = ?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{phone, pass});
        if (cursor.moveToFirst()) {
            String name = cursor.getString(1);
            cursor.close();
            db.close();
            return name;
        }
        return null;
    }

    public void addToPlaylist(SongItem songItem, String TABLE) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ALBUM_ID, songItem.getAlbumId());
        values.put(KEY_ALBUM, songItem.getAlbum());
        values.put(KEY_ARTIST, songItem.getArtist());
        values.put(KEY_TITLE, songItem.getTitle());
        values.put(KEY_DURATION, songItem.getDuration());
        values.put(KEY_PATH, songItem.getPath());

        db.insert(TABLE, null, values);
        db.close();
    }

    public void addToContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EMOTION_ID, contact.getEmotionId());
        values.put(KEY_NAME, contact.getName());
        values.put(KEY_PHONE, contact.getPhone());

        db.insert(TABLE_CONTACT, null, values);
        db.close();
    }

    public List<SongItem> getAllSongs(String mood) {
        List<SongItem> songs = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + mood;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                long album_id = cursor.getLong(1);
                String title = cursor.getString(2);
                String album = cursor.getString(3);
                String artist = cursor.getString(4);
                long duration = cursor.getLong(5);
                String path = cursor.getString(6);
                songs.add(new SongItem(title, artist, path, album, duration, album_id));
            } while (cursor.moveToNext());
            cursor.close();
            db.close();
        }
        return songs;
    }

    public List<Contact> getAllContacts(int emotion) {
        List<Contact> contacts = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_CONTACT + " WHERE emotion_id = ?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{String.valueOf(emotion)});
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String name = cursor.getString(2);
                String phone = cursor.getString(3);
                contacts.add(new Contact(id, name, phone));
            } while (cursor.moveToNext());
            cursor.close();
            db.close();
        }
        return contacts;
    }

    public void deleteSong(String mood, long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(mood, KEY_ALBUM_ID + "=" + id, null);
        db.close();
    }

    public void deleteContact(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACT, KEY_ID + "=" + id, null);
        db.close();
    }

    public int getSongsCount(String mood) {
        String countQuery = "SELECT * FROM " + mood;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();

        return count;
    }
}
