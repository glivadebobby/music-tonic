package in.glivade.musictonic;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.glivade.musictonic.db.DatabaseHandler;
import in.glivade.musictonic.util.MyPreference;

public class LoginActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextInputEditText editTextPhone, editTextPass;
    private Button buttonLogin;
    private TextView textViewRegister;
    private DatabaseHandler handler;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initObjects();
        initToolbar();
        initCallbacks();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        editTextPhone = findViewById(R.id.phone);
        editTextPass = findViewById(R.id.pass);
        buttonLogin = findViewById(R.id.btn_login);
        textViewRegister = findViewById(R.id.txt_register);

        handler = new DatabaseHandler(this);
        preference = new MyPreference(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initCallbacks() {
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = editTextPhone.getText().toString().trim();
                String pass = editTextPass.getText().toString().trim();
                if (phone.isEmpty() || phone.length() < 10 || pass.isEmpty()) {
                    Snackbar.make(buttonLogin, "Phone/Password is invalid", Snackbar.LENGTH_SHORT).show();
                } else {
                    String name = handler.signIn(phone, pass);
                    if (name == null) {
                        Snackbar.make(buttonLogin, "Invalid credentials", Snackbar.LENGTH_SHORT).show();
                    } else {
                        preference.setName(name);
                        preference.setIsLoggedIn(true);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }
                }
            }
        });
        textViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }
}
