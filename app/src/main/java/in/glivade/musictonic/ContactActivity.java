package in.glivade.musictonic;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.glivade.musictonic.db.DatabaseHandler;
import in.glivade.musictonic.model.Contact;

public class ContactActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Spinner spinnerEmotions;
    private RecyclerView viewContacts;
    private FloatingActionButton buttonAdd;
    private List<Contact> contacts;
    private ContactAdapter adapter;
    private DatabaseHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        initObjects();
        initToolbar();
        initCallbacks();
        initRecyclerView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) onBackPressed();
        return true;
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        spinnerEmotions = findViewById(R.id.emotions);
        viewContacts = findViewById(R.id.contacts);
        buttonAdd = findViewById(R.id.fab_add);

        contacts = new ArrayList<>();
        adapter = new ContactAdapter(contacts);
        handler = new DatabaseHandler(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        spinnerEmotions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                contacts.clear();
                contacts.addAll(handler.getAllContacts(position));
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertAddContact();
            }
        });
    }

    private void initRecyclerView() {
        viewContacts.setLayoutManager(new LinearLayoutManager(this));
        viewContacts.setAdapter(adapter);

        contacts.addAll(handler.getAllContacts(0));
        adapter.notifyDataSetChanged();
    }

    @SuppressLint("InflateParams")
    private void alertAddContact() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Contact");

        final View contactView = LayoutInflater.from(this).inflate(R.layout.dialog_add_contact, null);
        builder.setView(contactView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        final Spinner spinnerEmotions = contactView.findViewById(R.id.emotions);
        final TextInputEditText editTextName = contactView.findViewById(R.id.name);
        final TextInputEditText editTextPhone = contactView.findViewById(R.id.phone);
        Button buttonAdd = contactView.findViewById(R.id.btn_add_contact);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString().trim();
                String phone = editTextPhone.getText().toString().trim();
                if (name.isEmpty() || phone.isEmpty() || phone.length() < 10) {
                    Snackbar.make(contactView, "One or more fields are invalid", Snackbar.LENGTH_SHORT).show();
                } else {
                    handler.addToContact(new Contact(-1, spinnerEmotions.getSelectedItemPosition(), name, phone));
                    contacts.clear();
                    contacts.addAll(handler.getAllContacts(ContactActivity.this.spinnerEmotions.getSelectedItemPosition()));
                    adapter.notifyDataSetChanged();
                    alertDialog.dismiss();
                }
            }
        });
    }

    private void deleteAlert(final int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Contact");
        builder.setMessage("Are you sure you want to remove the contact?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handler.deleteContact(id);
                contacts.clear();
                contacts.addAll(handler.getAllContacts(spinnerEmotions.getSelectedItemPosition()));
                adapter.notifyDataSetChanged();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactHolder> {

        private List<Contact> contactList;

        ContactAdapter(List<Contact> contactList) {
            this.contactList = contactList;
        }

        @NonNull
        @Override
        public ContactHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ContactHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_contact, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ContactHolder holder, int position) {
            Contact contact = contactList.get(position);
            holder.textViewName.setText(contact.getName());
            holder.textViewPhone.setText(contact.getPhone());
        }

        @Override
        public int getItemCount() {
            return contactList.size();
        }

        class ContactHolder extends RecyclerView.ViewHolder {

            private TextView textViewName, textViewPhone;
            private Button buttonDelete;

            ContactHolder(View itemView) {
                super(itemView);
                textViewName = itemView.findViewById(R.id.txt_name);
                textViewPhone = itemView.findViewById(R.id.txt_phone);
                buttonDelete = itemView.findViewById(R.id.btn_delete);
                buttonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteAlert(contactList.get(getLayoutPosition()).getId());
                    }
                });
            }
        }
    }
}
