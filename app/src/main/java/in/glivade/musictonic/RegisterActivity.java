package in.glivade.musictonic;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import in.glivade.musictonic.db.DatabaseHandler;

public class RegisterActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextInputEditText editTextName, editTextEmail, editTextPhone, editTextPass;
    private Button buttonRegister;
    private DatabaseHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initObjects();
        initToolbar();
        initCallbacks();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) onBackPressed();
        return true;
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        editTextName = findViewById(R.id.name);
        editTextEmail = findViewById(R.id.email);
        editTextPhone = findViewById(R.id.phone);
        editTextPass = findViewById(R.id.pass);
        buttonRegister = findViewById(R.id.btn_register);

        handler = new DatabaseHandler(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString().trim();
                String email = editTextEmail.getText().toString().trim();
                String phone = editTextPhone.getText().toString().trim();
                String pass = editTextPass.getText().toString().trim();
                if (name.isEmpty() || email.isEmpty() || phone.isEmpty() || phone.length() < 10 || pass.isEmpty()) {
                    Snackbar.make(buttonRegister, "One or more fields are invalid", Snackbar.LENGTH_SHORT).show();
                } else {
                    handler.addUser(name, email, phone, pass);
                    Toast.makeText(RegisterActivity.this, "Successfully registered. Please login now.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }
}
